package com.example.pojo;

public enum DiscountType {
	AMOUNTOFF("AmountOff"),PERCENTAGEOFF("PercentageOff"),FIXEDPRICE("FixedPrice"),FREE("Free");
	
	private String discountType;
	private DiscountType(String discountType) {
		this.discountType=discountType;
	}
	
	String getDiscountType() {
		return discountType;
	}
	
}
