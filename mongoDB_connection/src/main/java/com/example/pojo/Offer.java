package com.example.pojo;

public class Offer {
private DiscountType discountType;
private double discountedAmount;
public DiscountType getDiscountType() {
	return discountType;
}
public void setDiscountType(DiscountType discountType) {
	this.discountType = discountType;
}
public double getDiscountedAmount() {
	return discountedAmount;
}
public void setDiscountedAmount(double discountedAmount) {
	this.discountedAmount = discountedAmount;
}

}
