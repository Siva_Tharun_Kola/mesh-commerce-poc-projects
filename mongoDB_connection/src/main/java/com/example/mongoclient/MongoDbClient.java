package com.example.mongoclient;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.example.pojo.Promotion;

public class MongoDbClient {
	
	public void insertIntoMongoDB(Promotion promotion,MongoTemplate mongoTemplate) {
		MongoOperations mongoOperations=(MongoOperations)mongoTemplate;
		mongoOperations.save(promotion);
		
	}
	
	Query searchpromotionQuery = new Query(Criteria.where("promotionType").is("ORDER"));
	
	public void updateIntoMongoDB(MongoTemplate mongoTemplate) {
		MongoOperations mongoOperations=(MongoOperations)mongoTemplate;
		mongoOperations.updateFirst(searchpromotionQuery, Update.update("priorityOfPromotion" ,5), Promotion.class);
	}
	
	public void removeFromMongoDB(MongoTemplate mongoTemplate) {
		MongoOperations mongoOperations=(MongoOperations)mongoTemplate;
		mongoOperations.remove(searchpromotionQuery,Promotion.class);
	} 

}