package com.example.mongodbconfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;

@Configuration
@ImportResource("classpath:/configuration.xml")
public class MongoDbConfiguration {
	
	@Bean(name="mongoTemplate")
	public MongoTemplate getMongoTemplate() {
		MongoTemplate mongoTemplate = new MongoTemplate(new SimpleMongoDbFactory(new MongoClient(), "meshPromotions"));
		return mongoTemplate;
	}
}
