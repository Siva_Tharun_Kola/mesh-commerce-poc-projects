package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.example.mongoclient.MongoDbClient;
import com.example.mongodbconfiguration.MongoDbConfiguration;
import com.example.pojo.Promotion;

@SpringBootApplication
public class MongoDbConnectionApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MongoDbConfiguration.class);
		MongoTemplate mongoTemplate =(MongoTemplate) applicationContext.getBean("mongoTemplate");
		MongoDbClient mongoDbClient=new MongoDbClient();
		Promotion promotion=(Promotion) applicationContext.getBean("promotion");
		
		mongoDbClient.insertIntoMongoDB(promotion,mongoTemplate);
		//mongoDbClient.updateIntoMongoDB(mongoTemplate);
		//mongoDbClient.removeFromMongoDB(mongoTemplate);
	}
	
}
