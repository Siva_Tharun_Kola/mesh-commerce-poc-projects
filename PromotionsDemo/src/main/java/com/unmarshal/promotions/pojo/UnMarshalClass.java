package com.unmarshal.promotions.pojo;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.promotions.pojo.PromotionDefinition;


public class UnMarshalClass {
	
	public PromotionDefinition unMarshallXML(File definitionFile) {
		PromotionDefinition definition = null;
        try {         
            JAXBContext context = JAXBContext.newInstance(PromotionDefinition.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            definition = (PromotionDefinition) unmarshaller.unmarshal(definitionFile);
     } catch (JAXBException e) {
            
     }
     return definition;
	}
}
