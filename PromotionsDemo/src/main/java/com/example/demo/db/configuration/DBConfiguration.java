package com.example.demo.db.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;

@Configuration
@PropertySource("classpath:/mongo-db.properties")
@ImportResource("classpath:/beanconfiguration.xml")
public class DBConfiguration {
	
	@Autowired
	private Environment environment;
	
	@Bean(name="mongoTemplate")
	public MongoTemplate getMongoTemplate() {
		MongoTemplate mongoTemplate = new MongoTemplate(new SimpleMongoDbFactory(new MongoClient(), environment.getProperty("com.demo.mongo.db")));
		return mongoTemplate;
	}
	
}
