package com.example.demo;

import java.io.File;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.demo.db.client.MongoClient;
import com.example.demo.db.configuration.DBConfiguration;
import com.promotions.pojo.PromotionDefinition;
import com.unmarshal.promotions.pojo.UnMarshalClass;

@SpringBootApplication
public class PromotionsDemoApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(DBConfiguration.class);
		MongoTemplate mongoTemplate =(MongoTemplate) applicationContext.getBean("mongoTemplate");
		UnMarshalClass unMarshalClass=(UnMarshalClass) applicationContext.getBean("unMarshalClass");
		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		String filename="promotions.xml";
		File file = new File(classLoader.getResource(filename).getFile());
		PromotionDefinition promotionDefinition=unMarshalClass.unMarshallXML(file);
		MongoClient mongoClient=(MongoClient)applicationContext.getBean("mongoClient");
		
		mongoClient.insertIntoMongoDB(promotionDefinition,mongoTemplate);
		
		//mongoClient.updateIntoMongoDB(mongoTemplate);
		
		//mongoClient.removeFromMongoDB(mongoTemplate);
	}
}
