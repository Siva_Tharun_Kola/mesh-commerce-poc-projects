package com.demo.db.client;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.promotions.pojo.PromotionDefinition;

public class MongoClient {
	
	public void insertIntoMongoDB(PromotionDefinition promotion,MongoTemplate mongoTemplate) {
		MongoOperations mongoOperations=(MongoOperations)mongoTemplate;
		mongoOperations.save(promotion);
		
	}
	
	Query searchpromotionQuery = new Query(Criteria.where("promotionId").is(1234));
	
	public void updateIntoMongoDB(MongoTemplate mongoTemplate) {
		MongoOperations mongoOperations=(MongoOperations)mongoTemplate;
		mongoOperations.updateFirst(searchpromotionQuery, Update.update("priority" ,10), PromotionDefinition.class);
	}
	
	public void removeFromMongoDB(MongoTemplate mongoTemplate) {
		MongoOperations mongoOperations=(MongoOperations)mongoTemplate;
		mongoOperations.remove(searchpromotionQuery,PromotionDefinition.class);
	} 

}
