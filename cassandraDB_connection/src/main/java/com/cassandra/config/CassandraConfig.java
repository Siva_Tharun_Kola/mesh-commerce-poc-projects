/*package com.cassandra.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.config.java.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.convert.CassandraConverter;
import org.springframework.data.cassandra.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

import com.datastax.driver.core.Session;

@Configuration
@PropertySource(value = { "classpath:cassandra.properties" })
@EnableCassandraRepositories(basePackages={"com.cassandra.repository"})
@ImportResource("classpath:/beanconfiguration.xml")
public class CassandraConfig extends AbstractCassandraConfiguration {

	@Autowired
	private Environment environment;
	
	@Value("${cassandra.contactpoints}")
	private String cassandraPoints;
	
	@Value("${cassandra.port}")
	private String cassandraPort;
	
	@Value("${cassandra.keyspace}")
	private String cassandraKeySpace;
	
	@Override
	protected String getKeyspaceName() {
		//return environment.getProperty("cassandra.keyspace");
		return cassandraKeySpace;
	}
	
	@Override
    @Bean
    public CassandraClusterFactoryBean cluster() {
        final CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
        cluster.setContactPoints(environment.getProperty("cassandra.contactpoints"));
        cluster.setPort(Integer.parseInt(environment.getProperty("cassandra.port")));
        cluster.setContactPoints(cassandraPoints);
        cluster.setPort(Integer.parseInt(cassandraPort));
        return cluster;
    }
	
	@Bean
    public CassandraSessionFactoryBean session() throws ClassNotFoundException {
 
        CassandraSessionFactoryBean session = new CassandraSessionFactoryBean();
        session.setCluster(cluster().getObject());
        session.setKeyspaceName(getKeyspaceName());
        session.setConverter(converter());
        session.setSchemaAction(SchemaAction.NONE);
        return session;
    }
	
	@Override
	@Bean
	public CassandraMappingContext cassandraMapping() throws ClassNotFoundException {
		return new BasicCassandraMappingContext();
	}
	 
	@Bean
	public CassandraConverter converter() throws ClassNotFoundException {
		return new MappingCassandraConverter(cassandraMapping());
	}
	
	@Bean
	public CassandraTemplate cassandraTemplate(Session session, CassandraConverter converter) throws Exception {
		return new CassandraTemplate(session, converter);
	}
}
*/