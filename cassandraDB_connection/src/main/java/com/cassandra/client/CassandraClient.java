package com.cassandra.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.naming.ConfigurationException;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;

import com.cassandra.test.model.Book;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.datastax.driver.core.utils.UUIDs;
import com.google.common.collect.ImmutableSet;

public class CassandraClient {

    @Autowired
    private CassandraTemplate cassandraTemplate;
    
    @Autowired
    private CassandraAdminOperations adminTemplate;

	/*public CassandraPromotion getCassandraPromotion(Promotion promotion) {
		OfferModel offerModel=new OfferModel();
		TermsAndConditionsModel termsAndConditionsModel=new TermsAndConditionsModel();
		DiscountTypeModel discountModel=new DiscountTypeModel();
		
		CassandraPromotion cassandraPromotion=new CassandraPromotion();
		//promotion pojo objects
		//promotionModel.setPromotionType(promotion.getPromotionType().toString());
		cassandraPromotion.setDescription(promotion.getDescription());
		cassandraPromotion.setDisplayPromoText(promotion.getDisplayPromoText());
		cassandraPromotion.setEnabled(promotion.isEnabled());
		cassandraPromotion.setEndDateOfthePromotion(promotion.getEndDateOfThepromotion());
		cassandraPromotion.setStartDateofThePromotion(promotion.getStartDateOfThePromotion());
		cassandraPromotion.setMaxUsersPerCustomer(promotion.getMaxUsesPerCustomer());
		cassandraPromotion.setNameofThePromotion(promotion.getNameOfThePromotion());
		cassandraPromotion.setPriorityOfPromotion(promotion.getPriorityOfPromotion());
		cassandraPromotion.setPromotionid(promotion.getPromotionId());
		termsAndConditionsModel.setItemsToBuy(promotion.getTermsAndConditions().getItemsToBuy());
		termsAndConditionsModel.setItemsToDiscount(promotion.getTermsAndConditions().getItemsToDiscount());
		termsAndConditionsModel.setNofOfItemsToBut(promotion.getTermsAndConditions().getNoOfItemsToBuy());
		termsAndConditionsModel.setNoofItemsToDiscount(promotion.getTermsAndConditions().getNoOfItemsToDiscount());
		cassandraPromotion.setTermsAndConditions(termsAndConditionsModel);
		offerModel.setDiscountedAmount(promotion.getOffer().getDiscountedAmount());
		discountModel.setDiscountType(promotion.getOffer().getDiscountType().toString());
		offerModel.setDiscountTypeModel(discountModel);
		cassandraPromotion.setOffer(offerModel);
		return cassandraPromotion;
	}
    
	public CassandraPromotion searchFromCassandraKeySpace(Promotion promotion) {
		Where select=QueryBuilder.select().from("promotion").where(QueryBuilder.eq("promotionid", promotion.getPromotionId()));
		CassandraPromotion resultPromotionModel = cassandraTemplate.selectOne(select, CassandraPromotion.class);
		return resultPromotionModel;
	}
	
	public void updateIntoCassandraTable(Promotion promotion) {
		
		CassandraPromotion cassandraPromotion=searchFromCassandraKeySpace(promotion);
		cassandraPromotion.setDescription("This Promotion is Updated");
		cassandraTemplate.update(cassandraPromotion);
	}
	
	public void removeFromCassandraTable(Promotion promotion) {
		CassandraPromotion cassandraPromotion=searchFromCassandraKeySpace(promotion);
		cassandraTemplate.delete(cassandraPromotion);
	} 
	
	public void InsertIntoCassandraTable(Promotion promotion) {
		CassandraPromotion cassandraPromotion= getCassandraPromotion(promotion);
		try {
		cassandraTemplate.insert(cassandraPromotion);
		} catch(Exception e) {
			
		}
	}*/

    @Before
    public void createTable() throws InterruptedException, ConfigurationException, IOException {
        adminTemplate.createTable(true, CqlIdentifier.cqlId(""), Book.class, new HashMap<String, Object>());
    }

    public void whenSavingBook_thenAvailableOnRetrieval() {
        final Book javaBook = new Book(UUIDs.timeBased(), "Head First Java", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        cassandraTemplate.insert(javaBook);
        final Select select = QueryBuilder.select().from("book").where(QueryBuilder.eq("title", "Head First Java")).and(QueryBuilder.eq("publisher", "O'Reilly Media")).limit(10);
        final Book retrievedBook = cassandraTemplate.selectOne(select, Book.class);
    }

    public void whenSavingBooks_thenAllAvailableOnRetrieval() {
        final Book javaBook = new Book(UUIDs.timeBased(), "Head First Java", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        final Book dPatternBook = new Book(UUIDs.timeBased(), "Head Design Patterns", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        final List<Book> bookList = new ArrayList<>();
        bookList.add(javaBook);
        bookList.add(dPatternBook);
        cassandraTemplate.insert(bookList);

        final Select select = QueryBuilder.select().from("book").limit(10);
        final List<Book> retrievedBooks = cassandraTemplate.select(select, Book.class);
    }

    public void whenUpdatingBook_thenShouldUpdatedOnRetrieval() {
        final Book javaBook = new Book(UUIDs.timeBased(), "Head First Java", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        cassandraTemplate.insert(javaBook);
        final Select select = QueryBuilder.select().from("book").limit(10);
        final Book retrievedBook = cassandraTemplate.selectOne(select, Book.class);
        retrievedBook.setTags(ImmutableSet.of("Java", "Programming"));
        cassandraTemplate.update(retrievedBook);
        final Book retrievedUpdatedBook = cassandraTemplate.selectOne(select, Book.class);
    }

    public void whenDeletingASelectedBook_thenNotAvailableOnRetrieval() throws InterruptedException {
        final Book javaBook = new Book(UUIDs.timeBased(), "Head First Java", "OReilly Media", ImmutableSet.of("Computer", "Software"));
        cassandraTemplate.insert(javaBook);
        cassandraTemplate.delete(javaBook);
        final Select select = QueryBuilder.select().from("book").limit(10);
        final Book retrievedUpdatedBook = cassandraTemplate.selectOne(select, Book.class);
    }

    public void whenDeletingAllBooks_thenNotAvailableOnRetrieval() {
        final Book javaBook = new Book(UUIDs.timeBased(), "Head First Java", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        final Book dPatternBook = new Book(UUIDs.timeBased(), "Head Design Patterns", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        cassandraTemplate.insert(javaBook);
        cassandraTemplate.insert(dPatternBook);
        cassandraTemplate.delete(Book.class);
        final Select select = QueryBuilder.select().from("book").limit(10);
        final Book retrievedUpdatedBook = cassandraTemplate.selectOne(select, Book.class);
    }

    public void whenAddingBooks_thenCountShouldBeCorrectOnRetrieval() {
        final Book javaBook = new Book(UUIDs.timeBased(), "Head First Java", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        final Book dPatternBook = new Book(UUIDs.timeBased(), "Head Design Patterns", "O'Reilly Media", ImmutableSet.of("Computer", "Software"));
        cassandraTemplate.insert(javaBook);
        cassandraTemplate.insert(dPatternBook);
        final long bookCount = cassandraTemplate.count(Book.class);
    }

}
