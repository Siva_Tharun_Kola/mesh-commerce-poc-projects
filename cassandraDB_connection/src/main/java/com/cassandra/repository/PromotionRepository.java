package com.cassandra.repository;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;
import org.springframework.stereotype.Repository;

import com.cassandra.model.CassandraPromotion;

@Repository
public interface PromotionRepository extends TypedIdCassandraRepository<CassandraPromotion,Integer> {
	
	@Query("select * from promotion where promotionid = ?0")
	Iterable<CassandraPromotion> findByPromotionId(String promotionId);
	

}
