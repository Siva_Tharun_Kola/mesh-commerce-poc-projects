package com.cassandra.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.UserDefinedType;

import com.datastax.driver.core.DataType.Name;

@UserDefinedType("termsandconditions")
public class TermsAndConditionsModel {
	
	@CassandraType(type=Name.INT)
	private int nofOfItemsToBut;
	
	@CassandraType(type=Name.INT)
	private int noofItemsToDiscount;
	
	@CassandraType(type=Name.LIST)
	private List<String> itemsToBuy=new ArrayList<>();
	
	@CassandraType(type=Name.LIST)
	private List<String> itemsToDiscount=new ArrayList<>();

	public int getNofOfItemsToBut() {
		return nofOfItemsToBut;
	}

	public void setNofOfItemsToBut(int nofOfItemsToBut) {
		this.nofOfItemsToBut = nofOfItemsToBut;
	}

	public int getNoofItemsToDiscount() {
		return noofItemsToDiscount;
	}

	public void setNoofItemsToDiscount(int noofItemsToDiscount) {
		this.noofItemsToDiscount = noofItemsToDiscount;
	}

	public List<String> getItemsToBuy() {
		return itemsToBuy;
	}

	public void setItemsToBuy(List<String> itemsToBuy) {
		this.itemsToBuy = itemsToBuy;
	}

	public List<String> getItemsToDiscount() {
		return itemsToDiscount;
	}

	public void setItemsToDiscount(List<String> itemsToDiscount) {
		this.itemsToDiscount = itemsToDiscount;
	}
	
}
