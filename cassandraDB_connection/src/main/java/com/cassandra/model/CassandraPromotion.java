package com.cassandra.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import com.datastax.driver.core.DataType.Name;

@Table("promotion")
public class CassandraPromotion implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PrimaryKeyColumn(name="promotionid")
	private int promotionid;
	
	@Column(value="nameofthepromotion")
	private String nameofThePromotion;
	
	@Column(value="description")
	private String description;
	
	@Column(value="displaypromotext")
	private String displayPromoText;
	
	@Column(value="startdateofthepromotion")
	private Date startDateofThePromotion;
	
	@Column(value="enddateofthepromotion")
	private Date endDateOfthePromotion;
	
	@Column(value="isenabled")
	private boolean isEnabled;

	@Column(value="maxuserspercustomer")
	private int maxUsersPerCustomer; 
	
	@Column
	@CassandraType(type = Name.UDT, userTypeName = "termsandconditions")
	private TermsAndConditionsModel termsAndConditions;
	
	@Column(value="priorityofpromotion")
	private int priorityOfPromotion;
	
	@Column
	@CassandraType(type = Name.UDT, userTypeName = "offer")
	private OfferModel offer;

	public int getPromotionid() {
		return promotionid;
	}

	public void setPromotionid(int promotionid) {
		this.promotionid = promotionid;
	}

	public String getNameofThePromotion() {
		return nameofThePromotion;
	}

	public void setNameofThePromotion(String nameofThePromotion) {
		this.nameofThePromotion = nameofThePromotion;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayPromoText() {
		return displayPromoText;
	}

	public void setDisplayPromoText(String displayPromoText) {
		this.displayPromoText = displayPromoText;
	}

	public Date getStartDateofThePromotion() {
		return startDateofThePromotion;
	}

	public void setStartDateofThePromotion(Date startDateofThePromotion) {
		this.startDateofThePromotion = startDateofThePromotion;
	}

	public Date getEndDateOfthePromotion() {
		return endDateOfthePromotion;
	}

	public void setEndDateOfthePromotion(Date endDateOfthePromotion) {
		this.endDateOfthePromotion = endDateOfthePromotion;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public int getMaxUsersPerCustomer() {
		return maxUsersPerCustomer;
	}

	public void setMaxUsersPerCustomer(int maxUsersPerCustomer) {
		this.maxUsersPerCustomer = maxUsersPerCustomer;
	}

	public TermsAndConditionsModel getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(TermsAndConditionsModel termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public int getPriorityOfPromotion() {
		return priorityOfPromotion;
	}

	public void setPriorityOfPromotion(int priorityOfPromotion) {
		this.priorityOfPromotion = priorityOfPromotion;
	}

	public OfferModel getOffer() {
		return offer;
	}

	public void setOffer(OfferModel offer) {
		this.offer = offer;
	}
}
