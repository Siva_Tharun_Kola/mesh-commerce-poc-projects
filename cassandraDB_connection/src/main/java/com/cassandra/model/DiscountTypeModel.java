package com.cassandra.model;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.UserDefinedType;

import com.datastax.driver.core.DataType.Name;

@UserDefinedType("discounttype")
public class DiscountTypeModel {
	@CassandraType(type=Name.VARCHAR)
	private String discountType;

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	
}
