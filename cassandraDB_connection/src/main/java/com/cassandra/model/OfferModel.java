package com.cassandra.model;

import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.UserDefinedType;

import com.datastax.driver.core.DataType.Name;

@UserDefinedType("offer")
public class OfferModel {
	@CassandraType(type = Name.UDT, userTypeName = "discounttype")
	private DiscountTypeModel discountTypeModel;
	
	@CassandraType(type=Name.DOUBLE)
	private double discountedAmount;

	public DiscountTypeModel getDiscountTypeModel() {
		return discountTypeModel;
	}

	public void setDiscountTypeModel(DiscountTypeModel discountTypeModel) {
		this.discountTypeModel = discountTypeModel;
	}

	public double getDiscountedAmount() {
		return discountedAmount;
	}

	public void setDiscountedAmount(double discountedAmount) {
		this.discountedAmount = discountedAmount;
	}
}
