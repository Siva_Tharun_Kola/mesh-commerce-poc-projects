package com.cassandra.promotion.pojo;

public enum OfferType {
	THRID_PARTY("ThirdParty"),BUY_X_GET_Y("BuyXgetY"),BIC("Bic"),BICBOGO("BicBogo"),SIMPLE("Simple");
	private String offerType;
	
	private OfferType(String offerType) {
		this.offerType=offerType;
	}

	String getOfferType() {
		return offerType;
	}

}
