package com.cassandra.promotion.pojo;

public enum PromotionType {
	SHIPPING("Shipping"),ORDER("Order"),COMMERCE_ITEM("CommerceItem");
	
	private String promotionType;
	
	PromotionType(String promotionType) {
		this.promotionType=promotionType;
	}
	
	String getPromotionType() {
		return promotionType;
	}
}
