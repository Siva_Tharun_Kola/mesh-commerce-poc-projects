package com.cassandra.promotion.pojo;

import java.util.List;

public class TermsAndConditions {
	private int noOfItemsToBuy;
	private int noOfItemsToDiscount;
	private  List<String> itemsToBuy;
	private List<String> itemsToDiscount;
	public int getNoOfItemsToBuy() {
		return noOfItemsToBuy;
	}
	public void setNoOfItemsToBuy(int noOfItemsToBuy) {
		this.noOfItemsToBuy = noOfItemsToBuy;
	}
	public int getNoOfItemsToDiscount() {
		return noOfItemsToDiscount;
	}
	public void setNoOfItemsToDiscount(int noOfItemsToDiscount) {
		this.noOfItemsToDiscount = noOfItemsToDiscount;
	}
	public List<String> getItemsToBuy() {
		return itemsToBuy;
	}
	public void setItemsToBuy(List<String> itemsToBuy) {
		this.itemsToBuy = itemsToBuy;
	}
	public List<String> getItemsToDiscount() {
		return itemsToDiscount;
	}
	public void setItemsToDiscount(List<String> itemsToDiscount) {
		this.itemsToDiscount = itemsToDiscount;
	}
	
}
