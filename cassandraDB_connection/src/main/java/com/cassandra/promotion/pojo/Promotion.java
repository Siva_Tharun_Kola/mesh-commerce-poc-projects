package com.cassandra.promotion.pojo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

public class Promotion {
	private int promotionId;
	private String nameOfThePromotion;
	private String description;
	private boolean isEnabled;
	private PromotionType promotionType;
	@Autowired
	private TermsAndConditions termsAndConditions;
	private boolean isApplicableToAllOrders;
	private Date startDateOfThePromotion;
	private Date endDateOfThepromotion;
	private int maxUsesPerCustomer;
	private String displayPromoText;
	private int priorityOfPromotion;
	private Offer offer;
	
	public String getNameOfThePromotion() {
		return nameOfThePromotion;
	}
	
	public void setNameOfThePromotion(String nameOfThePromotion) {
		this.nameOfThePromotion = nameOfThePromotion;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public PromotionType getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(PromotionType promotionType) {
		this.promotionType = promotionType;
	}
	public TermsAndConditions getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(TermsAndConditions termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	public boolean isApplicableToAllOrders() {
		return isApplicableToAllOrders;
	}
	public void setIsApplicableToAllOrders(boolean isApplicableToAllOrders) {
		this.isApplicableToAllOrders = isApplicableToAllOrders;
	}
	public Date getStartDateOfThePromotion() {
		return startDateOfThePromotion;
	}
	public void setStartDateOfThePromotion(Date startDateOfThePromotion) {
		this.startDateOfThePromotion = startDateOfThePromotion;
	}
	public Date getEndDateOfThepromotion() {
		return endDateOfThepromotion;
	}
	public void setEndDateOfThepromotion(Date endDateOfThepromotion) {
		this.endDateOfThepromotion = endDateOfThepromotion;
	}
	public int getMaxUsesPerCustomer() {
		return maxUsesPerCustomer;
	}
	public void setMaxUsesPerCustomer(int maxUsesPerCustomer) {
		this.maxUsesPerCustomer = maxUsesPerCustomer;
	}
	public String getDisplayPromoText() {
		return displayPromoText;
	}
	public void setDisplayPromoText(String displayPromoText) {
		this.displayPromoText = displayPromoText;
	}
	public int getPriorityOfPromotion() {
		return priorityOfPromotion;
	}
	public void setPriorityOfPromotion(int priorityOfPromotion) {
		this.priorityOfPromotion = priorityOfPromotion;
	}
	public Offer getOffer() {
		return offer;
	}
	public void setOffer(Offer offer) {
		this.offer = offer;
	}
	public boolean isEnabled() {
		return isEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	public int getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
}
