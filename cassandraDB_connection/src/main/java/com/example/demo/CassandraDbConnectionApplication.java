package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cassandra.client.CassandraClient;
import com.cassandra.config.CassandraConfigBook;
import com.cassandra.promotion.pojo.Promotion;

@SpringBootApplication
public class CassandraDbConnectionApplication {

	public static void main(String[] args) {
		//SpringApplication.run(CassandraDbConnectionApplication.class, args);
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(CassandraConfigBook.class);
		Promotion promotion=(Promotion) applicationContext.getBean("promotion");
		CassandraClient cassandraClient=new CassandraClient();
		//cassandraClient.InsertIntoCassandraTable(promotion);
		//cassandraClient.removeFromCassandraTable(promotion);
		//cassandraClient.updateIntoCassandraTable(promotion);
	}
	
	
}
